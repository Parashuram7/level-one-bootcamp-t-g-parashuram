//WAP to find the distance between two points using structures and 4 functions.

#include<stdio.h>
#include<math.h>

struct Point {
        float  x1,x2,y1,y2;
}point1,point2;

float input()
{
        printf("For First value:\n");
        printf("Enter the value for X:\n");
        scanf("%f",&point1.x1);
        printf("Enter the value for Y:\n");
        scanf("%f",&point1.y1);

        printf("For Second value:\n");
        printf("Enter the value for X:\n");
        scanf("%f",&point2.x2);
        printf("Enter the value for Y:\n");
        scanf("%f",&point2.y2);
}



float display(float firstPoint,float secondPoint,float Distance)
{
        printf("The distance between two points %f and %f  is: %f\n", firstPoint ,secondPoint, Distance);
}

float output()
{
      float firstPoint,secondPoint,Distance;
      firstPoint = point2.x2 - point1.x1;
      secondPoint = point2.y2 - point1.y1;
      Distance = (firstPoint*firstPoint + secondPoint);
      display(firstPoint,secondPoint,Distance);
      return Distance;
}

int main()
{ 
     input();
     output();

     return 0;
}


