//WAP to find the sum of n fractions.
#include<stdio.h>

struct Fraction
{
    int size, numerator[30], denominator[30];
}fraction;

int input()
{
    printf("Enter Size of Fractions to compute: \n");
    scanf("%d",&fraction.size);

    printf("Enter the Numerator Values: \n");
    for(int i=0; i < fraction.size; i++)
   {
    scanf("%d",&fraction.numerator[i]);
}

printf("Enter the Denominator Values: \n");
for(int i=0; i < fraction.size; i++)
{
     scanf("%d",&fraction.denominator[i]);
}
}
int gcd(int num, int den)
{
     if(den==0)
   {
     return num;
}

return gcd(den, num % den);
}

int LCM(int array[], int size)
{
     int lcm = array[0];
     for(int i=1;  i<size;  i++)
    {
    lcm = (((array[i]*lcm)) / (gcd(array[i],lcm)));
}

return lcm;
}

int display(int Num,int Den)
{
     printf("The final computation is %d/%d", Num, Den);
}

int  computeNFractions(int limit, int num[], int den[])
{
   int Numerator=0;
   int Denominator = LCM(den,limit);
   for(int i=0;i< limit;i++)
 {
Numerator = Numerator + (num[i]) * (Denominator/den[i]);
}

int GCD = gcd (Numerator , Denominator);

Numerator = Numerator/GCD;
Denominator = Denominator/GCD;

display(Numerator , Denominator);
}

int main()
{
    input();
    computeNFractions(fraction.size, fraction.numerator, fraction.denominator);
    return 0;
}

