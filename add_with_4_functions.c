//Write a program to add two user input numbers using 4 functions.

#include<stdio.h>
float input()
{
    float num;
    printf("Enter the number: \n");
    scanf("%f",&num);
    return num;
}
float sum(float num1,float num2)
{
   float sum;
   sum=num1+num2;
   return sum;
}
void output(float a,float b,float num)
{
   printf("sum of %f and %f is %f\n",a,b,num);
   return 0;
}
int main()
{
     float x,y,s;
     x=input();
     y=input();
     s=sum(x,y);
     output(x,y,s);
     return 0;
}
